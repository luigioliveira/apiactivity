const express = require("express");
const datetime = require("node-datetime")
const dt = datetime.create()
const app = express();
const bodyParser = require('body-parser');
const acceptableAccessTokens = ['LUIGI-TOKEN-0504', 'GOBBATO-TOKEN-0504']
let formattedDate = dt.format('m/d/Y H:M:S');

// Defining Router

var router = express.Router();

// Using body-parser

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

// Funcionários end-point

router.get("/", (req, res) => {
    let queryRequest = req.query
    console.log(`O endpoint de Funcionários está sendo acessado às {}`, formattedDate);

    let returnJSON = {
        body: {
            message: "You are now posting Clientes informations",
            resource: "GET-Funcionarios",
            nome: queryRequest.nome
        }
    }

    res.json(returnJSON);
});

router.post("/", (req, res) => {
    let headersRequest = req.headers;
    if (acceptableAccessTokens.includes(headersRequest.access)) {
        let bodyRequest = req.body;
        let returnJSON = {
            body: {
                message: "You are now posting Funcionários informations",
                resource: "POST-Funcionarios",
                nome: bodyRequest.nome,
                sobrenome: bodyRequest.sobrenome,
                idade: bodyRequest.idade
            },
            headers: {
                access: headersRequest.access
            }
        }
        console.log(`O endpoint de Funcionários está sendo acessado às `, formattedDate);
        console.log(bodyRequest);
        console.log(headersRequest);
        res.json(returnJSON);
    }
    else{
        let returnJSON = {
            body: {
                error: "You are access token is not registered in the platform, please contact administrator!",
                access: headersRequest.access
            }
        }
        res.json(returnJSON)
    }

});

router.delete("/:nome/:sobrenome/:idade", (req, res)=>{
    let headersRequest = req.headers;
    if (acceptableAccessTokens.includes(headersRequest.access)) {
        let parametersRequest = req.params;
        let returnJSON = {
            body: {
                message: "You are now deleting Funcionários informations",
                resource: "DELETE-Funcionarios",
                nome: parametersRequest['nome'],
                sobrenome: parametersRequest['sobrenome'],
                idade: parametersRequest['idade']
            },
            headers: {
                access: headersRequest.access
            }
        }
        console.log(`O endpoint de Funcionarios está sendo acessado às `, formattedDate);
        console.log(req.params);
        console.log(headersRequest);
        res.json(returnJSON);
    }
    else{
        let returnJSON = {
            body: {
                error: "You are access token is not registered in the platform, please contact administrator!",
                access: headersRequest.access
            }
        }
        res.json(returnJSON)
    }
});

router.put("/", (req, res) => {
    let headersRequest = req.headers;
    if (acceptableAccessTokens.includes(headersRequest.access)) {
        let bodyRequest = req.body;
        let returnJSON = {
            body: {
                message: "You are now posting Funcionarios informations",
                resource: "PUT-Funcionarios",
                nome: bodyRequest.nome,
                sobrenome: bodyRequest.sobrenome,
                idade: bodyRequest.idade
            },
            headers: {
                access: headersRequest.access
            }
        }
        console.log(`O endpoint de Funcionários está sendo acessado às `, formattedDate);
        console.log(bodyRequest);
        console.log(headersRequest);
        res.json(returnJSON);
    }
    else{
        let returnJSON = {
            body: {
                error: "You are access token is not registered in the platform, please contact administrator!",
                access: headersRequest.access
            }
        }
        res.json(returnJSON)
    }

});

module.exports.router = router;