const express = require("express");
const datetime = require("node-datetime")
const dt = datetime.create()
const app = express();
const port = 8001;
const server = "Local/UNICSUL"
let formattedDate = dt.format('m/d/Y H:M:S');

app.use('/clientes', require('./routes/clientes').router);
app.use('/funcionarios', require('./routes/funcionarios').router);

app.listen(port, () => {
    console.log("==============================================================");
    console.log("Projeto API Activity por Luigi Oliveira iniciado!");
    console.log("Projeto executando na porta: " + port);
    console.log("Servidor: " + server + " | Horário: " + formattedDate);
    console.log("==============================================================");
});

module.exports = app;